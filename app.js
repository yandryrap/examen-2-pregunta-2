const res = require("express/lib/response")

const express = requiere ('express')
const app = express()

const {body, validationResult} = requiere ('express-validator')

app.use(express.json())
app.set('view engine', 'ejs')
app.use(express.urlencoded({extended:true}))

app.get('/', (req,res)=>{
    res.render('index')
})

app.post('/datoclient', [
    body('ced', 'Ingrese su cédula')
        .exists()
        .isNumeric({min:10}),
    body('nom', 'Ingrese sus nombres')
        .exists()
        .islength(),
    body('direc', 'Ingrese su dirección')
        .exists()
        .islength(),
    body('telef', 'Ingrese su telefono')
        .exists()
        .isNumeric({min:10}),
    body('corr', 'Ingrese su correo electrónico')
        .exists()
        .isEmail()
],(req,res)=>{
    
    const errors = validationResult(req)
    if (!errors.isEmpty()){
        console.log(req.body)
        const datos = req.body //valores que se capturan
        const validaciones = error.array()
        res.render('index', {validaciones: validaciones, datos: datos}) //render de la página enviado valores
    }else {
        res.send('La validacion esta correcta')
        const result = await pool.query("INSERT INTO datos_cliente set ?", [req.body]);
    }
})

app.listen(3000, ()=>{
    console.log('SERVER UP en http://localhost:3000')
})
